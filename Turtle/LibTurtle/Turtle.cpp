#include "Turtle.h"
#include "SDL.h"
#include <iostream>
using namespace std;

constexpr float degPerRad = 57.2957795131f;
constexpr float radPerDeg = 0.01745329251f;

struct TurtleImpl
{
	SDL_Window* window = nullptr;
	int width = 700, height = 700;
	SDL_Renderer* renderer = nullptr;

	bool hide = false;
	Point position;
	float degrees = 0.0f;
	float facingDegrees = 0.0f;

	bool simulate = true;
	float normalSpeed = 200.0f;
	SDL_Texture* image = nullptr;
	SDL_Texture* canvas = nullptr;

	TurtleImpl() = default;

	void Init()
	{
		SDL_Init(SDL_INIT_VIDEO);

		window = SDL_CreateWindow("Turtle Graphics", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
		if (window == nullptr)
		{
			cout << "Error: Failed to create window." << endl;
			return;
		}

		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (renderer == nullptr)
		{
			cout << "Error: Failed to create renderer." << endl;
			return;
		}


		SDL_GetWindowSize(window, &width, &height);

		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		SDL_RenderClear(renderer);
		SDL_RenderPresent(renderer);

		SDL_Surface* surf = SDL_LoadBMP("Turtle.bmp");
		SDL_SetColorKey(surf, 1, SDL_MapRGB(surf->format, 255, 0, 255));

		image = SDL_CreateTextureFromSurface(renderer, surf);
		SDL_FreeSurface(surf);

		canvas = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, width, height);
		SDL_SetRenderTarget(renderer, canvas);
		SDL_RenderClear(renderer);

		SDL_SetRenderTarget(renderer, nullptr);
	}

	void Quit()
	{
		SDL_RenderPresent(renderer);

		SDL_DestroyTexture(canvas);
		SDL_DestroyTexture(image);
		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
		SDL_Quit();

		renderer = nullptr;
		window = nullptr;
	}

	int GetWidth() const
	{
		return width;
	}

	int GetHeight() const
	{
		return height;
	}

	void Process(Turtle& turtle)
	{
		if (turtle.pen.active)
		{
			// Draw pen on canvas
			SDL_SetRenderTarget(renderer, canvas);
			float s = turtle.pen.size;
			float s2 = s / 2;
			SDL_Rect rect = { int(position.x - s2), int(position.y - s2), int(s), int(s) };
			SDL_SetRenderDrawColor(renderer, turtle.pen.color.red, turtle.pen.color.green, turtle.pen.color.blue, turtle.pen.color.alpha);
			SDL_RenderFillRect(renderer, &rect);
			SDL_SetRenderTarget(renderer, nullptr);
		}

		// Draw canvas on screen
		SDL_RenderCopy(renderer, canvas, nullptr, nullptr);

		if (!hide)
		{
			// Draw turtle on screen
			SDL_Rect turtleRect = { int(position.x - 25), int(position.y - 25), 50, 50 };
			while (facingDegrees > 180)
				facingDegrees -= 180;

			if(facingDegrees >= -90 && facingDegrees <= 90)
				SDL_RenderCopyEx(renderer, image, nullptr, &turtleRect, facingDegrees, nullptr, SDL_FLIP_NONE);
			else
				SDL_RenderCopyEx(renderer, image, nullptr, &turtleRect, facingDegrees - 180.0, nullptr, SDL_FLIP_HORIZONTAL);
		}

		if (simulate)
		{
			SDL_RenderPresent(renderer);
			SDL_Delay(10);
		}
	}
};

Turtle::Turtle()
{
	impl = new TurtleImpl();
	impl->Init();
}

Turtle::~Turtle()
{
	impl->Quit();
	delete impl;
}

void Turtle::SetSimulate(bool enable)
{
	impl->simulate = enable;
}

void Turtle::Clear()
{
	SDL_SetRenderTarget(impl->renderer, impl->canvas);

	SDL_SetRenderDrawColor(impl->renderer, 255, 255, 255, 255);
	SDL_RenderClear(impl->renderer);

	SDL_SetRenderTarget(impl->renderer, nullptr);
}

void Turtle::Hide(bool enable)
{
	impl->hide = enable;
}


void Turtle::Save(const std::string& filename)
{
	Uint32 Rmask;
	Uint32 Gmask;
	Uint32 Bmask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	Rmask = 0xff0000;
	Gmask = 0x00ff00;
	Bmask = 0x0000ff;
#else
	Rmask = 0x0000ff;
	Gmask = 0x00ff00;
	Bmask = 0xff0000;
#endif

	SDL_Surface* surf = SDL_CreateRGBSurface(SDL_SWSURFACE, impl->width, impl->height, 24, Rmask, Gmask, Bmask, 0);

	SDL_Rect rect = {0, 0, impl->width, impl->height};
	SDL_SetRenderTarget(impl->renderer, impl->canvas);
	SDL_RenderReadPixels(impl->renderer, &rect, SDL_PIXELFORMAT_RGB24, surf->pixels, surf->pitch);
	SDL_SetRenderTarget(impl->renderer, nullptr);

	SDL_SaveBMP(surf, filename.c_str());
	SDL_FreeSurface(surf);
}

void Turtle::Forward(float distance)
{
	float dx = distance * cosf(impl->degrees * radPerDeg);
	float dy = distance * sinf(impl->degrees * radPerDeg);

	MoveTo(impl->position.x + dx, impl->position.y + dy);
}

void Turtle::TurnLeft(float degrees)
{
	impl->degrees -= degrees;
	impl->facingDegrees = impl->degrees;
}

void Turtle::TurnRight(float degrees)
{
	impl->degrees += degrees;
	impl->facingDegrees = impl->degrees;
}

void Turtle::MoveTo(float x, float y)
{
	float dx = x - impl->position.x;
	float dy = y - impl->position.y;
	float distance = sqrtf(dx * dx + dy * dy);
	impl->facingDegrees = atan2f(dy, dx) * degPerRad;

	float duration = distance / impl->normalSpeed;

	float timeStep = 0.016f;
	int steps = int(duration / timeStep);
	float dx_step = dx / steps;
	float dy_step = dy / steps;

	bool simulate = impl->simulate;

	for (int i = 0; i < steps - 1; ++i)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_SPACE)
					impl->simulate = false;
			}
		}

		impl->position = Point(impl->position.x + dx_step, impl->position.y + dy_step);
		impl->Process(*this);
	}

	// Hit the last point precisely
	impl->position = Point(x, y);
	impl->Process(*this);

	impl->simulate = simulate;
}

void Turtle::LookAt(float degrees)
{
	impl->degrees = degrees;
	impl->facingDegrees = impl->degrees;
}

void Turtle::Sleep(float seconds)
{
	SDL_RenderPresent(impl->renderer);
	SDL_Delay((Uint32)(seconds * 1000));
}

Point Turtle::GetPosition() const
{
	return impl->position;
}

float Turtle::GetHeading() const
{
	return impl->degrees;
}


int Turtle::GetBoundaryWidth() const
{
	return impl->GetWidth();
}

int Turtle::GetBoundaryHeight() const
{
	return impl->GetHeight();
}

bool Turtle::IsHidden() const
{
	return impl->hide;
}

bool Turtle::IsSimulating() const
{
	return impl->simulate;
}

float Turtle::GetSpeed() const
{
	return impl->normalSpeed;
}

void Turtle::SetSpeed(float speed)
{
	impl->normalSpeed = speed;
}