#include "Turtle.h"
#include "Random.h"
#include <iostream>
#include <math.h>
using namespace std;

//Creates A Square
void Square(Turtle& _turtle, Color _color, float sideLength)
{
	//No brush size provided so is defaulted to 5 pixels
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = 5;

	//Writes Square
	for (int i = 0; i < 4; i++)
	{
		_turtle.Forward(sideLength);
		_turtle.TurnRight(90);
	}

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}
void Square(Turtle& _turtle, Color _color, float sideLength, float brushSize)
{
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = brushSize;

	//Writes Square
	for (int i = 0; i < 4; i++)
	{
		_turtle.Forward(sideLength);
		_turtle.TurnRight(90);
	}

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}

//Creates A Triangle
void Triangle(Turtle& _turtle, Color _color, float sideALength, float sideBLength)
{
	//No angle provided so is defaulted to be 90 degrees
	//No brush size provided so is defaulted to 5 pixels
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = 5;

	//Calculates The Length Of The Final Side Of The Triangle
	float finalSide = hypot(sideALength, sideBLength);

	//Writes Sides 'A' And 'B'
	_turtle.Forward(sideALength);
	_turtle.TurnLeft(90);
	_turtle.Forward(sideBLength);

	//Writes Final Side
	_turtle.TurnLeft(135);
	_turtle.Forward(finalSide);

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}

void Triangle(Turtle& _turtle, Color _color, float sideALength, float sideBLength, float angleDeg)
{
	//No brush size provided so is defaulted to 5 pixels
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = 5;

	//Converts Angle From Degrees To Radians
	double pi = 3.1415926535897;
	float angleRad = angleDeg * pi/180;

	//Calculates The Length Of The Final Side Of The Triangle
	float finalSide = sqrtf((powf(sideALength, 2) + powf(sideBLength, 2)) - (2 * sideALength * sideBLength * cosf(angleRad)));

	//Calculates Angle Of The Final Side In Radians
	double otherAngleRad = sin(angleRad)*sideALength/finalSide;
	//Converts Radians To Degrees
	double otherAngleDeg = otherAngleRad * 180/pi;

	//Writes Sides 'A' And 'B'
	_turtle.Forward(sideALength);
	_turtle.TurnLeft(180);
	_turtle.TurnRight(angleDeg);
	_turtle.Forward(sideBLength);

	//Writes Final Side
	_turtle.TurnLeft(180);
	_turtle.TurnRight(otherAngleDeg);
	_turtle.Forward(finalSide);

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}
void Triangle(Turtle& _turtle, Color _color, float sideALength, float sideBLength, float angleDeg, float brushSize)
{
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = brushSize;

	//Converts Angle From Degrees To Radians
	double pi = 3.1415926535897;
	float angleRad = angleDeg * pi / 180;

	//Calculates The Length Of The Final Side Of The Triangle
	float finalSide = sqrtf((powf(sideALength, 2) + powf(sideBLength, 2)) - (2 * sideALength * sideBLength * cosf(angleRad)));

	//Calculates Angle Of The Final Side In Radians
	double otherAngleRad = sin(angleRad) * sideALength / finalSide;
	//Converts Radians To Degrees
	double otherAngleDeg = otherAngleRad * 180 / pi;

	//Writes Sides 'A' And 'B'
	_turtle.Forward(sideALength);
	_turtle.TurnLeft(180);
	_turtle.TurnRight(angleDeg);
	_turtle.Forward(sideBLength);

	//Writes Final Side
	_turtle.TurnLeft(180);
	_turtle.TurnRight(otherAngleDeg);
	_turtle.Forward(finalSide);

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}

//Creates An X
void Cross(Turtle& _turtle, Color _color, float length)
{
	//No brush size provided so is defaulted to 5 pixels
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = 5;

	_turtle.TurnRight(45);

	//Writes First Line
	_turtle.Forward(length);
	_turtle.pen.active = false;

	//Moves Into Position To Make Final Line
	_turtle.TurnRight(180);
	_turtle.Forward(length / 2);
	_turtle.TurnRight(90);
	_turtle.Forward(length / 2);

	//Writes Final Line
	_turtle.pen.active = true;
	_turtle.TurnRight(180);
	_turtle.Forward(length);

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}
void Cross(Turtle& _turtle, Color _color, float length, float brushSize)
{
	_turtle.pen.active = true;
	_turtle.pen.color = _color;
	_turtle.pen.size = brushSize;

	_turtle.TurnRight(45);

	//Writes First Line
	_turtle.Forward(length);
	_turtle.pen.active = false;

	//Moves Into Position To Make Final Line
	_turtle.TurnRight(180);
	_turtle.Forward(length/2);
	_turtle.TurnRight(90);
	_turtle.Forward(length/2);

	//Writes Final Line
	_turtle.pen.active = true;
	_turtle.TurnRight(180);
	_turtle.Forward(length);

	//Returns To Top Left Corner
	_turtle.Sleep(1.0f);
	_turtle.pen.active = false;
	_turtle.MoveTo(0, 0);
}

int main(int argc, char* argv[])
{
	Random random;

	Turtle turtle;

	turtle.MoveTo(turtle.GetBoundaryWidth() / 2.0f, turtle.GetBoundaryHeight() / 2.0f);

	//Makes Green Square
	Square(turtle, Color(0,255,0), 100);
	turtle.Save("Screenshot1.bmp");

	turtle.Clear();
	turtle.LookAt(0);
	turtle.MoveTo(turtle.GetBoundaryWidth() / 2.0f, turtle.GetBoundaryHeight() / 2.0f);

	//Makes Blue TriangleDeg
	Triangle(turtle, Color(0,0,255), 100, 100);
	turtle.Save("Screenshot2.bmp");

	turtle.Clear();
	turtle.LookAt(0);
	turtle.MoveTo(turtle.GetBoundaryWidth() / 2.0f, turtle.GetBoundaryHeight() / 2.0f);

	//Makes Purple X
	Cross(turtle, Color(255,0,255), 100, 10);
	turtle.Save("Screenshot3.bmp");

	return 0;
}