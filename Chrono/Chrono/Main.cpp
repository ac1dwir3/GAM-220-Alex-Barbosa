#include <iostream>
#include <chrono>
using namespace std;
using namespace chrono;

int main(int argc, char* argv[])
{
	//Calculate time it takes to write out message
	high_resolution_clock::time_point startTime = high_resolution_clock::now();
	cout << "Some message here." << endl;
	high_resolution_clock::time_point stopTime = high_resolution_clock::now();

	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << numSeconds << endl;

	//Calculate time it takes to add all numbers between 0 and 100000
	int j = 0;
	startTime = high_resolution_clock::now();
	for (int i = 0; i <= 100000; i++)
	{
		j += i;
	}
	stopTime = high_resolution_clock::now();

	totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	numSeconds = totalNS.count() / 1e9f;

	cout << numSeconds << endl;

	//Calculate time it takes to calculate if the number is odd
	int num = 1000000;
	startTime = high_resolution_clock::now();
	int remainder = num % 2;
	stopTime = high_resolution_clock::now();

	totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	numSeconds = totalNS.count() / 1e9f;

	cout << numSeconds << endl;

	return 0;
}