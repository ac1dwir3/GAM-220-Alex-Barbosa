#pragma once
#include "Shape.h"

class Circle : public Shape
{
public:
	Circle() = default;

	float segmentLength = 1.5f;

	virtual void Draw(float x, float y, Turtle& turtle) override
	{
		turtle.SetSimulate(false);
		turtle.LookAt(0);
		turtle.MoveTo(x, y);
		turtle.pen.color = GetColor();
		turtle.pen.size = 20;
		turtle.pen.active = true;

		//TODO: Writes Circle
		for (int i = 0; i < 180; i++)
		{
			turtle.Forward(segmentLength * GetScale());
			turtle.TurnRight(2);
		}

		//Returns To Top Left Corner
		turtle.pen.active = false;
		//turtle.MoveTo(0, 0);
		turtle.LookAt(0);

	}
};