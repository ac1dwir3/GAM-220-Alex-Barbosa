#pragma once
#include "Shape.h"

class Triangle : public Shape
{
public:
	float sideLength;

	Triangle(float size)
	{
		sideLength = size;
	}


	virtual void Draw(float x, float y, Turtle& turtle) override
	{
		turtle.SetSimulate(false);
		turtle.LookAt(0);
		turtle.MoveTo(x, y);
		turtle.pen.color = GetColor();
		turtle.pen.size = 20;
		turtle.pen.active = true;

		//Writes Triangle
		turtle.Forward(sideLength * GetScale());
		turtle.TurnLeft(135);
		turtle.Forward(sideLength * GetScale());
		turtle.TurnLeft(90);
		turtle.Forward(sideLength * GetScale());

		//Closes Gap
		turtle.LookAt(0);
		turtle.Forward(sideLength * GetScale());

		//Returns To Top Left Corner
		turtle.pen.active = false;
		//turtle.MoveTo(0, 0);
		turtle.LookAt(0);

	}
};