#pragma once
#include "Shape.h"
#include "Turtle.h"

class Sprite
{
public:
	Shape* shape;

	Sprite() = default;
	Sprite(Shape& inputShape, float _x, float _y, float xVel, float yVel)
	{
		shape = &inputShape;
		x = _x;
		y = _y;
		xVelocity = xVel;
		yVelocity = yVel;
	};

	void Update(Turtle& turtle)
	{
		//If Hits Left Or Right Walls
		if (x >= turtle.GetBoundaryWidth() || x <= 0)
			SetVelocity(-xVelocity, yVelocity); //Send Other Direction
		//If Hits Top Or Bottom Walls
		else if (y >= turtle.GetBoundaryHeight() || y <= 0)
			SetVelocity(xVelocity, -yVelocity); //Send Other Direction

		SetPosition(x + xVelocity, y + yVelocity); //Updates Postion
		Draw(turtle); //Draws Shape

	};

	void Draw(Turtle& turtle)
	{
		shape->Draw(x, y, turtle);
	};

	void SetPosition(float _x, float _y)
	{
		x = _x;
		y = _y;
	};

	Point GetPostition()
	{
		return Point(x, y);
	};

	void SetVelocity(float xVel, float yVel)
	{
		xVelocity = xVel;
		yVelocity = yVel;
	};

	Point GetVelocity()
	{
		return Point(xVelocity, yVelocity);
	};


private:
	float x = 0.0f;
	float y = 0.0f;
	float xVelocity = 0.0f;
	float yVelocity = 0.0f;

};