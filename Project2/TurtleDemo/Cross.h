#pragma once
#include "Shape.h"

class Cross : public Shape
{
public:
	float sideLength;

	Cross(float size)
	{
		sideLength = size;
	};


	virtual void Draw(float x, float y, Turtle& turtle) override
	{
		turtle.SetSimulate(false);
		turtle.LookAt(0);
		turtle.MoveTo(x, y);
		turtle.pen.color = GetColor();
		turtle.pen.size = 20;
		turtle.pen.active = true;

		turtle.TurnRight(45);

		//Writes First Line
		turtle.Forward(sideLength * GetScale());
		turtle.pen.active = false;

		//Moves Into Position To Make Final Line
		turtle.TurnRight(180);
		turtle.Forward((sideLength * GetScale()) / 2);
		turtle.TurnRight(90);
		turtle.Forward((sideLength * GetScale()) / 2);

		//Writes Final Line
		turtle.pen.active = true;
		turtle.TurnRight(180);
		turtle.Forward(sideLength * GetScale());

		//Returns To Top Left Corner
		turtle.pen.active = false;
		//turtle.MoveTo(0, 0);
		turtle.LookAt(0);
	}
};