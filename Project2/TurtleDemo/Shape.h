#pragma once
#include "Turtle.h"

class Shape
{
public:
	Shape() = default;

	void SetScale(float newScale)
	{
		scale = newScale;
	}

	float GetScale()
	{
		return scale;
	}

	void SetColor(Color newColor)
	{
		color = newColor;
	}

	Color GetColor()
	{
		return color;
	}

	virtual void Draw(float x, float y, Turtle& turtle) abstract;

private:
	float scale = 1.0f;
	Color color = Color(0,0,0,255);
};