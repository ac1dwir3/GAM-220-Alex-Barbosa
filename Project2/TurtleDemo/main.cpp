#include "Turtle.h"
#include "Sprite.h"
#include "Shape.h"
#include "Square.h"
#include "Circle.h"
#include "Triangle.h"
#include "Cross.h"
#include "Random.h"

int main(int argc, char* argv[])
{
	Turtle turtle;
	Random random;

	turtle.Hide(true);

	Square* square = new Square(100);
	square->SetColor(Color(213, 159, 209)); //Pink
	Triangle* triangle = new Triangle(100);
	triangle->SetColor(Color(138, 207, 179)); //Green
	Circle* circle = new Circle();
	circle->SetColor(Color(223, 104, 134)); //Red
	Cross* cross = new Cross(100);
	cross->SetColor(Color(167, 170, 232)); //Blue

	Shape* shapeTypes[4] = { square, triangle, circle, cross }; //PlayStation Icons

	int amountOfShapes = random.Integer(3, 12);
	Sprite** sprites = new Sprite*[amountOfShapes];
	for (int i = 0; i < amountOfShapes; i++)
	{
		sprites[i] = new Sprite(*shapeTypes[random.Integer(0, 3)], random.Float(0, turtle.GetBoundaryWidth()), random.Float(0, turtle.GetBoundaryHeight()), random.Float(-150.0f, 150.0f), random.Float(-150.0f, 150.0f));
		
		if (sprites[i]->GetVelocity().x == 0)
			sprites[i]->SetVelocity(1, sprites[i]->GetVelocity().y);
		else if (sprites[i]->GetVelocity().y == 0)
			sprites[i]->SetVelocity(sprites[i]->GetVelocity().x, 1);

	}

	while (true)
	{
		for (int i = 0; i < amountOfShapes; i++)
			sprites[i]->Update(turtle);

		turtle.Sleep(0.005f);
		turtle.Clear();
	}

	return 0;
}