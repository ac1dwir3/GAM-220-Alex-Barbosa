#pragma once
#include "Shape.h"

class Square : public Shape
{
public:
	float sideLength;

	Square(float size)
	{
		sideLength = size;
	};


	virtual void Draw(float x, float y, Turtle& turtle) override
	{
		turtle.SetSimulate(false);
		turtle.LookAt(0);
		turtle.MoveTo(x, y);
		turtle.pen.color = GetColor();
		turtle.pen.size = 20;
		turtle.pen.active = true;

		//Writes Square
		for (int i = 0; i < 4; i++)
		{
			turtle.Forward(sideLength * GetScale());
			turtle.TurnRight(90);
		}

		//Returns To Top Left Corner
		turtle.pen.active = false;
		//turtle.MoveTo(0, 0);
		turtle.LookAt(0);
	}
};