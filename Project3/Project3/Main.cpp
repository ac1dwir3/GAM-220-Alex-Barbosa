#include <iostream>
#include <string>
#include "Vector.h"
using namespace std;

void PrintVector(const Vector<int>& myVector)
{
	for (int i = 0; i < myVector.Size(); ++i)
	{
		cout << myVector[i] << " ";
	}
	cout << endl << endl;
}
void PrintVector(Vector<char>* myVector)
{
	for (int i = 0; i < myVector->Size(); ++i)
	{
		cout << myVector->At(i) << " ";
	}
	cout << endl << endl;
}

int main(int argc, char* argv[])
{
	Vector<int> intVector(2);
	PrintVector(intVector);

	intVector.PushFront(5);
	PrintVector(intVector);

	intVector.PushBack(3);
	PrintVector(intVector);

	intVector.At(1) = 99;
	intVector[2] = 10;
	PrintVector(intVector);

	cout << "Size: " << intVector.Size() << endl;

	cout << "Element at index 2: " << intVector.At(2) << endl;
	PrintVector(intVector);

	intVector.Clear();
	PrintVector(intVector);

	intVector.Resize(9);
	PrintVector(intVector);

	intVector.Reserve(11);
	PrintVector(intVector);

	cout << "Capacity: " << intVector.GetCapacity() << endl;

	intVector[2] = 12;
	intVector[8] = 2000;
	intVector[3] = 876;

	if(intVector.Contains(12))
		cout << "Contains 12 = true" << endl;
	else
		cout << "Contains 12 = false" << endl;
	if (intVector.Contains(999))
		cout << "Contains 999 = true" << endl;
	else
		cout << "Contains 999 = false" << endl;
	PrintVector(intVector);

	if (intVector.Find(12) > -1)
		cout << "12 found at index: " << intVector.Find(12) << endl;
	else
		cout << "12 not found" << endl;
	if (intVector.Find(999) > -1)
		cout << "999 found at index: " << intVector.Find(12) << endl;
	else
		cout << "999 not found" << endl;
	PrintVector(intVector);

	if (intVector.Find(12) > -1)
		intVector.Erase(12);
	else
		cout << "12 not found" << endl;
	if (intVector.Find(999) > -1)
		intVector.Erase(999);
	else
		cout << "999 not found" << endl;
	PrintVector(intVector);

	intVector.Insert(999, 1);
	PrintVector(intVector);

	//___________________________________________________________________________________________________

	Vector<char>* charVector = new Vector<char>(2);
	PrintVector(charVector);

	charVector->PushFront('A');
	PrintVector(charVector);

	charVector->PushBack('Z');
	PrintVector(charVector);

	charVector->At(1) = 'M';
	charVector->At(2) = '$';
	PrintVector(charVector);

	cout << "Size: " << charVector->Size() << endl;

	cout << "Element at index 2: " << charVector->At(2) << endl;
	PrintVector(charVector);

	charVector->Clear();
	PrintVector(charVector);

	charVector->Resize(9);
	PrintVector(charVector);

	charVector->Reserve(11);
	PrintVector(charVector);

	cout << "Capacity: " << charVector->GetCapacity() << endl;

	charVector->At(2) = 'E';
	charVector->At(8) = 'H';
	charVector->At(3) = 'L';

	if (charVector->Contains('E'))
		cout << "Contains E = true" << endl;
	else
		cout << "Contains E = false" << endl;
	if (charVector->Contains('P'))
		cout << "Contains P = true" << endl;
	else
		cout << "Contains P = false" << endl;
	PrintVector(charVector);

	if (charVector->Find('E') > -1)
		cout << "E found at index: " << charVector->Find('E') << endl;
	else
		cout << "E not found" << endl;
	if (charVector->Find('P') > -1)
		cout << "P found at index: " << charVector->Find('P') << endl;
	else
		cout << "P not found" << endl;
	PrintVector(charVector);

	if (charVector->Find('E') > -1)
		charVector->Erase('E');
	else
		cout << "E not found" << endl;
	if (charVector->Find('P') > -1)
		charVector->Erase('P');
	else
		cout << "P not found" << endl;
	PrintVector(charVector);

	charVector->Insert('P', 1);
	PrintVector(charVector);


	cin.get();
	return 0;
}
