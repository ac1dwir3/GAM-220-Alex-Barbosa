#pragma once
#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Vector
{
public:
	Vector()
	{
		size = 0;
		data = new T[size];
	}

	Vector(int vectorSize)
	{
		size = vectorSize;
		data = new T[size];
	}

	~Vector()
	{
		delete[] data;
	}

	void PushFront(T value)
	{
		int newSize = size + 1;
		T* tempData = data;
		data = new T[newSize];

		data[0] = value;

		for (int i = 0; i < size; i++)
			data[i + 1] = tempData[i];

		size = newSize;

		cout << value << " added to the front." << endl;
	}

	void PushBack(T value)
	{
		int newSize = size + 1;
		T* tempData = data;
		data = new T[newSize];

		for (int i = 0; i < size; i++)
			data[i] = tempData[i];

		data[size] = value;

		size = newSize;

		cout << value << " added to back." << endl;
	}

	T& At(int index)
	{
		if (index < 0 || index >= size)
			throw std::out_of_range("Vector::At() index out of bounds!");
		return data[index];
	}
	const T& At(int index) const
	{
		if (index < 0 || index >= size)
			throw std::out_of_range("Vector::At() index out of bounds!");
		return data[index];
	}

	T& operator[](int index)
	{
		return At(index);
	}
	const T& operator[](int index) const
	{
		return At(index);
	}

	void Clear()
	{
		data = new T[0];
		size = 0;

		cout << "Data cleared." << endl;
	}

	int Size() const
	{
		return size;
	}

	bool IsEmpty()
	{
		if (Size() = 0)
			return true;

		return false;
	}

	void Resize(int newSize)
	{
		T* tempData = data;
		data = new T[newSize];

		for (int i = 0; i < newSize; i++)
			data[i] = tempData[i];

		size = newSize;

		cout << "Data resized to " << newSize << " elements." << endl;
	}

	void Reserve(int newCapacity)
	{
		if (newCapacity >= size)
			Resize(newCapacity);
		else
			throw std::out_of_range("Vector::Reserve(int newCapacity) newCapacity cannot be smaller than vector size or data will be lost! Use Vector::Resize(int newSize) instead!");
	}

	int GetCapacity()
	{
		return Size();
	}

	void EraseAt(int index)
	{
		if (index >= 0 && index < size)
		{
			int newSize = size - 1;
			T* tempData = data;
			data = new T[newSize];

			for (int i = 0; i < newSize; i++)
			{
				for (int j = 0; j < size; j++)
				{
					if (j == index)
						j++;

					data[i] = tempData[j];
					i++;
				}
			}

			size = newSize;

			cout << "Removed element at index " << index << "." << endl;
		}
		else
			throw std::out_of_range("Vector::EraseAt(int index) index out of bounds!");
	}

	void Erase(T value)
	{
		if (Contains(value))
		{
			EraseAt(Find(value));
		}
	}

	int Find(T value)
	{
		if (Contains(value))
		{
			for (int i = 0; i < size; i++)
			{
				if (data[i] == value)
					return i;
			}
		}
		return -1;
	}

	bool Contains(T value)
	{
		for (int i = 0; i < size; i++)
		{
			if (data[i] == value)
			{
				return true;
			}
		}
		return false;
	}

	void Insert(T value, int index)
	{
		if (index >= 0)
		{
			int newSize = size + 1;
			T* tempData = data;
			data = new T[newSize];

			for (int i = 0; i < newSize; i++)
			{
				for (int j = 0; j < size; j++)
				{
					if (j == index)
					{
						data[i] = value;
						j++;
						i++;
					}

					data[i] = tempData[j];
					i++;
				}
			}

			size = newSize;

			cout << value << " inserted at index " << index << "." << endl;
		}
		else
			throw std::out_of_range("Vector::Insert(T value, int index) index out of bounds!");

	}

private:
	T* data;
	int size;
};