#include "Console.h"
#include "Game.h"
using namespace std;


int main(int argc, char* argv[])
{
	bool stopPlaying = false;
	do
	{
		//Title
		cout << "   --Battleship--" << endl;
		//Waiting for player to start the game
		cout << "Press Enter to Begin" << endl;
		Console::WaitForEnter();
		Console::Clear();

		//Creating a "Game" object to reference
		Game game;

		//Begins ship placing procedure
		game.PlaceShips();

		bool player1Turn = true;
			
		

		bool stillPlaying = true;
		while (stillPlaying)
		{
			//Allows For Players To Enter Their Names
			string name = player1Turn ? game.GetPlayerName(true) : game.GetPlayerName(false);

			//Waits for the current player to begin their turn
			Console::Clear();
			cout << name << ", press Enter to continue." << endl;
			Console::WaitForEnter();

			//Displays the boards
			game.PrintBoards(player1Turn);

			//Allows the current player to choose a target to fire at
			int row, column;
			while (!PromptForCoordinates("Choose coordinates to fire (e.g. B7)", row, column))
			{//empty
			}

			//Registers the results of the player's shot
			FireResult result = game.Fire(player1Turn, row, column);

			//Displays the updated boards
			game.PrintBoards(player1Turn);

			//Checks if the player's shot was successful
			if (result.hit)
			{
				//display "HIT!!!"
				cout << endl << "HIT!!!" << endl;
				//Checks if the ship that was hit has been sunk; if so, display which ship the player sank
				if (result.sank != Ship::None)
					cout << "You sank the " << GetShipString(result.sank) << endl;

				// Check opposite player's board to see if they have no more ships to sink
				if (game.HasNoShips(!player1Turn))
				{
					//Display which player won
					cout << endl << name + " wins!" << endl;

					//Asks if players want to restart
					bool responded = false;
					do
					{
						cout << "Restart? Y/N" << endl;
						string response;
						getline(cin, response);
						if (response == "Y" || response == "y")
						{
							stopPlaying = false;
							responded = true;
						}
						else if (response == "N" || response == "n")
						{
							stopPlaying = true;
							responded = true;
						}
					} while (!responded);

					break;
				}
			}
			else
				//If the player's shot missed, display "Miss..."
				cout << endl << "Miss..." << endl;

			//Waits for input to continue
			cout << "Press Enter to Continue." << endl;
			Console::WaitForEnter();

			//Switched which player's turn it is
			player1Turn = !player1Turn;
		}

	} while (!stopPlaying);

	Console::WaitForEnter();

	return 0;
}