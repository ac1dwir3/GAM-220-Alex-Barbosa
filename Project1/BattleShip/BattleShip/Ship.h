#pragma once
#include <string>

enum class Ship
{
	None,
	Carrier,
	Battleship,
	Cruiser,
	Submarine,
	Destroyer
};

extern const Ship ships[];


int GetShipSize(Ship ship);
char GetShipSymbol(Ship ship);
std::string GetShipString(Ship ship);
Ship GetShipFromSymbol(char symbol);