/*#include "Console.h"
#include "Game.h"
using namespace std;

int main(int argc, char* argv[])
{
	bool stopPlaying = false;
	do
	{
		Game test;
		bool player1 = true;

		//Allows For Players To Enter Their Names
		string name = player1Turn ? game.GetPlayerName(true) : game.GetPlayerName(false);

		//Waits for the current player to begin their turn
		Console::Clear();
		cout << name << ", press Enter to continue." << endl;
		Console::WaitForEnter();

		//Testing Manually Placing Ships And Quitting While Placing Ships
		//test.PlaceShips();

		//Fills Player Board Same As Enemy Board
		test.PlaceShip(player1, Ship::Carrier, 0, 0, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Battleship, 1, 0, Alignment::Vertical);
		test.PlaceShip(player1, Ship::Cruiser, 1, 1, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Submarine, 5, 5, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Destroyer, 9, 4, Alignment::Horizontal);

		player1 = false;

		//Fills Enemy Board
		test.PlaceShip(player1, Ship::Carrier, 0, 0, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Battleship, 1, 0, Alignment::Vertical);
		test.PlaceShip(player1, Ship::Cruiser, 1, 1, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Submarine, 5, 5, Alignment::Horizontal);
		test.PlaceShip(player1, Ship::Destroyer, 9, 4, Alignment::Horizontal);

		//Test Hits
		test.Fire(player1, 0, 0);
		test.Fire(player1, 0, 1);
		test.Fire(player1, 0, 2);
		test.Fire(player1, 0, 3);
		test.Fire(player1, 0, 4);
		test.Fire(player1, 1, 1);
		test.Fire(player1, 1, 2);
		test.Fire(player1, 1, 3);
		test.Fire(player1, 1, 0);
		test.Fire(player1, 2, 0);
		test.Fire(player1, 3, 0);
		test.Fire(player1, 4, 0);
		test.Fire(player1, 5, 5);
		test.Fire(player1, 5, 6);
		test.Fire(player1, 5, 7);
		test.Fire(player1, 9, 4);

		//Test Misses
		test.Fire(player1, 4, 5);
		test.Fire(player1, 7, 5);
		test.Fire(player1, 3, 9);
		test.Fire(player1, 7, 6);

		//Test Shots At Places Already Shot At
		test.Fire(player1, 0, 0);
		test.Fire(player1, 1, 3);
		test.Fire(player1, 7, 6);


		test.PrintBoards(player1);

		//Below Is Used For Manual Testing
		//Used When Testing FireAt() And Win Condition

		int row, column;
		bool stillPlaying = true;

		while (stillPlaying)
		{
			while (!PromptForCoordinates("Fire at J6 to sink the final ship and test the hit and win condition. Fire anywhere else to test misses and places you've already fired at. Press 'Q' to quit", row, column))
			{
			}
			//Registers the results of the player's shot
			FireResult result = test.Fire(player1, row, column);

			//Checks if the player's shot was successful
			if (result.hit)
			{
				//display "HIT!!!"
				cout << "HIT!!!" << endl << endl;
				//Checks if the ship that was hit has been sunk; if so, display which ship the player sank
				if (result.sank != Ship::None)
					cout << "You sank the " << GetShipString(result.sank) << endl << endl;

				// Check opposite player's board to see if they have no more ships to sink
				if (test.HasNoShips(!player1))
				{
					//Display which player won
					cout << name + " wins!" << endl;

					//Asks if players want to restart
					bool responded = false;
					do
					{
						cout << "Restart? Y/N" << endl;
						string response;
						getline(cin, response);
						if (response == "Y" || response == "y")
						{
							stopPlaying = false;
							responded = true;
						}
						else if (response == "N" || response == "n")
						{
							stopPlaying = true;
							responded = true;
						}
					} while (!responded);

					break;
				}
			}
			else
				//If the player's shot missed, display "Miss..."
				cout << "Miss..." << endl << endl;

			//Displays the updated boards
			test.PrintBoards(player1);
		}

	}while (!stopPlaying);

	return 0;
}*/