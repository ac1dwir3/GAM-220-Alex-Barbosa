#include "Board.h"

#include <iostream>
using namespace std;

Board::Board()
{
	for (int i = 0; i < 100; i++)
		grid[i] = GetShipSymbol(Ship::None);
}

bool Board::IsSpaceOpen(int row, int column) const
{
	bool isOpen = false;

	int index = column + (row * 10);

	if (grid[index] == GetShipSymbol(Ship::None))
		isOpen = true;

	return isOpen;
}

bool Board::PlaceShip(Ship ship, int row, int column, Alignment alignment)
{
	bool isBlocked = false;

	int sectionsLeft = GetShipSize(ship);

	bool isEdgeR = false;
	bool isEdgeB = false;

	switch (alignment)
	{
	case Alignment::Horizontal:

		while (sectionsLeft != 0)
		{

			int index = column + (row * 10);

			if (column == 10)
				isEdgeR = true;

			if (index < 100)
			{
				if (isEdgeR == false && sectionsLeft > 0 && grid[index] == GetShipSymbol(Ship::None))
				{
					SetSpace(row, column, GetShipSymbol(ship));
					sectionsLeft--;
					column++;
				}
				else if ((isEdgeR == true && sectionsLeft > 0) || grid[index] != GetShipSymbol(Ship::None))
				{
					isBlocked = true;
					break;
				}
			}
			else
			{
				isBlocked = true;
				break;
			}
		}

		if (isBlocked == true)
		{
			cout << "Cannot place " << GetShipString(ship) << " in desired location..." << endl;

			for (int i = 0; i < 100; i++)
				if (grid[i] == GetShipSymbol(ship))
					grid[i] = GetShipSymbol(Ship::None);
		}
		else
			cout << "Ship succesfully placed" << endl;

		return !isBlocked;
	
	case Alignment::Vertical:

		while (sectionsLeft != 0)
		{

			int index = column + (row * 10);
			
			if (row == 10)
				isEdgeB = true;

			if (index < 100)
			{
				if (isEdgeB == false && sectionsLeft > 0 && grid[index] == GetShipSymbol(Ship::None))
				{
					SetSpace(row, column, GetShipSymbol(ship));
					sectionsLeft--;
					row++;
				}
				else if ((isEdgeB == true && sectionsLeft > 0) || grid[index] != GetShipSymbol(Ship::None))
				{
					isBlocked = true;
					break;
				}
			}
		}

		if (isBlocked == true)
		{
			cout << "Cannot place " << GetShipString(ship) << " in desired location..." << endl;

			for(int i = 0; i<100; i++)
				if (grid[i] == GetShipSymbol(ship))
					grid[i] = GetShipSymbol(Ship::None);
		}
		else
			cout << "Ship succesfully placed" << endl;

		return !isBlocked;
	}
}

FireResult Board::FireAt(int row, int column)
{
	int index = column + (row * 10);
	FireResult result;

	if (grid[index] != GetShipSymbol(Ship::None) && grid[index] != 'O' && grid[index] != 'X')
	{
		bool hit = true;
		Ship shipHit = GetShipFromSymbol(grid[index]);
		bool stillAfloat = false;


		SetSpace(row, column, 'X');

		for (int i = 0; i < 100; i++)
		{
			if (grid[i] == GetShipSymbol(shipHit))
			{
				result = FireResult(hit, Ship::None);
				stillAfloat = true;
				break;
			}
		}

		if (!stillAfloat)
			result = FireResult(hit, shipHit);
	}
	else if (grid[index] == GetShipSymbol(Ship::None))
		SetSpace(row, column, 'O');
	else
		cout << "You've already shot there!!!" << endl;

	return result;
}

char Board::GetSpace(int row, int column) const
{
	int index = column + (row * 10);

	return grid[index];
}

bool Board::HasShip(Ship ship) const
{
	bool hasShip = false;

	for (int i = 0; i < 100; i++)
	{
		if (grid[i] == GetShipSymbol(ship))
		{
			hasShip = true;
			break;
		}
	}

	return hasShip;
}

bool Board::HasNoShips() const
{
	bool noShips = true;

	for (int i = 0; i < 100; i++)
	{
		int row = i / 10;
		int col = i % 10;

		switch (GetSpace(row, col))
		{
		case 'C':
			noShips = false;
			break;
		case 'B':
			noShips = false;
			break;
		case 'c':
			noShips = false;
			break;
		case 'S':
			noShips = false;
			break;
		case 'D':
			noShips = false;
			break;
		}
	}

	return noShips;
}

void Board::SetSpace(int row, int column, char value)
{
	int index = column + (row * 10);

	grid[index] = value;
}