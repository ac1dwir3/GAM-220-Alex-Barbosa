#include "Ship.h"


const Ship ships[] = { Ship::Carrier, Ship::Battleship, Ship::Cruiser, Ship::Submarine, Ship::Destroyer };


int GetShipSize(Ship ship)
{
	switch (ship)
	{
	case Ship::Carrier:
		return 5;
	case Ship::Battleship:
		return 4;
	case Ship::Cruiser:
		return 3;
	case Ship::Submarine:
		return 3;
	case Ship::Destroyer:
		return 2;
	default:
		return 0;
	}
}

char GetShipSymbol(Ship ship)
{
	switch (ship)
	{
	case Ship::Carrier:
		return 'C';
	case Ship::Battleship:
		return 'B';
	case Ship::Cruiser:
		return 'c';
	case Ship::Submarine:
		return 'S';
	case Ship::Destroyer:
		return 'D';
	default:
		return '_';
	}
}

std::string GetShipString(Ship ship)
{
	switch (ship)
	{
	case Ship::Carrier:
		return "Carrier";
	case Ship::Battleship:
		return "Battleship";
	case Ship::Cruiser:
		return "Cruiser";
	case Ship::Submarine:
		return "Submarine";
	case Ship::Destroyer:
		return "Destroyer";
	default:
		return "N/A";
	}
}

Ship GetShipFromSymbol(char symbol)
{
	switch (symbol)
	{
	case 'C':
		return Ship::Carrier;
	case 'B':
		return Ship::Battleship;
	case 'c':
		return Ship::Cruiser;
	case 'S':
		return Ship::Submarine;
	case 'D':
		return Ship::Destroyer;
	default:
		return Ship::None;
	}
}