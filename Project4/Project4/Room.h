#pragma once
#include "Character.h"
#include "Monster.h"

class Room
{
public:
	Room()
	{
		name = "Placeholder Room Name";
		northRoom = nullptr;
		eastRoom = nullptr;
		southRoom = nullptr;
		westRoom = nullptr;
		monster = nullptr;
	}
	Room(std::string newRoomName)
	{
		name = newRoomName;
		northRoom = nullptr;
		eastRoom = nullptr;
		westRoom = nullptr;
		southRoom = nullptr;
		monster = nullptr;
	}
	Room(Monster* newMonster)
	{
		name = "Placeholder Room Name";
		northRoom = nullptr;
		eastRoom = nullptr;
		westRoom = nullptr;
		southRoom = nullptr;
		monster = newMonster;
	}
	Room(std::string newRoomName, Monster* newMonster)
	{
		name = newRoomName;
		northRoom = nullptr;
		eastRoom = nullptr;
		westRoom = nullptr;
		southRoom = nullptr;
		monster = newMonster;
	}
	Room(Room* newNorthRoom, Room* newEastRoom, Room* newSouthRoom, Room* newWestRoom)
	{
		name = "PlaceholderRoomName";
		northRoom = newNorthRoom;
		eastRoom = newEastRoom;
		southRoom = newSouthRoom;
		westRoom = newWestRoom;
		monster = nullptr;
	}
	Room(std::string newRoomName, Room* newNorthRoom, Room* newEastRoom, Room* newSouthRoom, Room* newWestRoom)
	{
		name = newRoomName;
		northRoom = newNorthRoom;
		eastRoom = newEastRoom;
		southRoom = newSouthRoom;
		westRoom = newWestRoom;
		monster = nullptr;
	}
	Room(Room* newNorthRoom, Room* newEastRoom, Room* newSouthRoom, Room* newWestRoom, Monster* newMonster)
	{
		name = "PlaceholderRoomName";
		northRoom = newNorthRoom;
		eastRoom = newEastRoom;
		southRoom = newSouthRoom;
		westRoom = newWestRoom;
		monster = newMonster;
	}
	Room(std::string newRoomName, Room* newNorthRoom, Room* newEastRoom, Room* newSouthRoom, Room* newWestRoom, Monster* newMonster)
	{
		name = newRoomName;
		northRoom = newNorthRoom;
		eastRoom = newEastRoom;
		southRoom = newSouthRoom;
		westRoom = newWestRoom;
		monster = newMonster;
	}

	std::string GetName()
	{
		return name;
	}

	Room* GetNorthRoom()
	{
		return northRoom;
	}
	Room* GetEastRoom()
	{
		return eastRoom;
	}
	Room* GetSouthRoom()
	{
		return southRoom;
	}
	Room* GetWestRoom()
	{
		return westRoom;
	}
	Monster* GetMonster()
	{
		return monster;
	}

	void SetNorthRoom(Room* newNorthRoom)
	{
		northRoom = newNorthRoom;
	}
	void SetEastRoom(Room* newEastRoom)
	{
		eastRoom = newEastRoom;
	}
	void SetSouthRoom(Room* newSouthRoom)
	{
		southRoom = newSouthRoom;
	}
	void SetWestRoom(Room* newWestRoom)
	{
		westRoom = newWestRoom;
	}
	void SetMonster(Monster* newMonster)
	{
		monster = newMonster;
	}

	bool hasMonster()
	{
		if (monster != nullptr)
			return true;

		return false;
	}

private:
	std::string name;
	Room* northRoom;
	Room* eastRoom;
	Room* southRoom;
	Room* westRoom;
	Monster* monster;
};