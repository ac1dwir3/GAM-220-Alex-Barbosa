#pragma once
#include "Random.h"
#include <string>
#include <iostream>

enum class CharacterType
{
	Human,
	Ogre,
	Vampire,
	Minotaur,
	Werewolf,
	Cerberus,
	Gravelord
};

class Character
{
public:
	Character(std::string newName, CharacterType newType)
	{
		name = newName;
		type = newType;
		switch (type)
		{
		case CharacterType::Human:
			hitpoints = 100;
			strength = 15;
			defense = 10;
			speed = 15;
			break;
		case CharacterType::Ogre:
			hitpoints = 80;
			strength = 30;
			defense = 20;
			speed = 5;
			break;
		case CharacterType::Vampire:
			hitpoints = 50;
			strength = 12;
			defense = 5;
			speed = 25;
			break;
		case CharacterType::Minotaur:
			hitpoints = 100;
			strength = 20;
			defense = 5;
			speed = 10;
			break;
		case CharacterType::Werewolf:
			hitpoints = 90;
			strength = 15;
			defense = 10;
			speed = 20;
			break;
		case CharacterType::Cerberus:
			hitpoints = 110;
			strength = 15;
			defense = 5;
			speed = 25;
			break;
		case CharacterType::Gravelord:
			hitpoints = 1000;
			strength = 50;
			defense = 30;
			speed = 1;
			break;
		}
		alive = true;
	}

	std::string GetName()
	{
		return name;
	}
	void SetName(std::string newName)
	{
		name = newName;
	}

	CharacterType GetType()
	{
		return type;
	}
	std::string GetTypeName()
	{
		switch (type)
		{
		case CharacterType::Human:
			return "Human";
			break;
		case CharacterType::Ogre:
			return "Ogre";
			break;
		case CharacterType::Vampire:
			return "Vampire";
			break;
		case CharacterType::Minotaur:
			return "Minotaur";
			break;
		case CharacterType::Werewolf:
			return "Werewolf";
			break;
		default:
			return "Placeholder Type Name";
			break;
		}
	}
	void SetCharacterType(CharacterType newType)
	{
		type = newType;
	}

	int GetHitPoints()
	{
		return hitpoints;
	}
	void  SetHitPoints(int newHealth)
	{
		hitpoints = newHealth;
	}
	
	int GetStrength() const
	{
		return strength;
	}
	void  SetStrength(int newStrength)
	{
		strength = newStrength;
	}

	int GetDefense() const
	{
		return defense;
	}
	void  GetDefense(int newDefense)
	{
		defense = newDefense;
	}
	
	int GetSpeed() const
	{
		return speed;
	}
	void  SetSpeed(int newSpeed)
	{
		speed = newSpeed;
	}

	bool IsAlive()
	{
		return alive;
	}
	void SetAlive(bool newLife)
	{
		alive = newLife;
	}
	std::string IsAliveString()
	{
		if (alive)
			return "Alive";

		return "Dead";
	}
	void Dead()
	{
		alive = false;
	}

	void DoAttack(Character* attacker, Character* defender, Random& rnd)
	{
		int randomNumber = rnd.Integer(0, 100);

		if (randomNumber < 10 * (1 + (defender->GetSpeed() / 10)))
		{
			std::cout << attacker->GetName() << " Attacks " << defender->GetName() << " But " << defender->GetName() << " Is Too Quick!" << std::endl;
			return;
		}

		// Gets Damage
		int damage = attacker->GetStrength() - defender->GetDefense();

		// Announce The Attack
		std::cout << attacker->GetName() << " Attacks " << defender->GetName() << ", Striking For " << damage << " Damage." << std::endl;

		// Make It Hurt
		defender->Hurt(damage);

		// Say what the effect was
		std::cout << defender->GetName() << " Has " << defender->GetHitPoints() << " Health Left." << std::endl;
	}

	void Hurt(int damage)
	{
		if (damage < 0)
			damage = 0;

		hitpoints -= damage;
		if (hitpoints < 0)
		{
			hitpoints = 0;
			Dead();
		}
	}

protected:
	std::string name;
	CharacterType type;
	int hitpoints;
	int strength;
	int defense;
	int speed;
	bool alive;
};