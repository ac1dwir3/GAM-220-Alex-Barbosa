#pragma once
#include "Character.h"

class Monster : public Character
{
public:
	Monster(CharacterType newType)
		: Character("Placeholder Monster Name", newType)
	{
		switch (newType)
		{
		case CharacterType::Human:
			SetName("Michael");
			break;
		case CharacterType::Ogre:
			SetName("Barlog");
			break;
		case CharacterType::Vampire:
			SetName("Harkon");
			break;
		case CharacterType::Minotaur:
			SetName("Asterion");
			break;
		case CharacterType::Werewolf:
			SetName("Sinding");
			break;
		case CharacterType::Cerberus:
			SetName("Doggo");
			break;
		case CharacterType::Gravelord:
			SetName("Nito");
			break;
		}
	}
	Monster(std::string newName, CharacterType newType)
		: Character(newName, newType)
	{}


private:

};