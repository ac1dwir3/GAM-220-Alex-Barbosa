#include "Player.h"
#include "Character.h"
#include "Room.h"
#include "Monster.h"
#include "Random.h"
#include <iostream>
#include <string>
using namespace std;

static void DescribeRoom(Room* room)
{
	//Empty Discriptions
	std::string northDescription;
	std::string eastDescription;
	std::string southDescription;
	std::string westDescription;
	std::string monsterDescription;

	//Generates Discriptions
	if (room->GetNorthRoom() != nullptr)
		northDescription = room->GetNorthRoom()->GetName() + " Is To The North";
	else
		northDescription = "There Is No Room To The North";

	if (room->GetEastRoom() != nullptr)
		eastDescription = room->GetEastRoom()->GetName() + " Is To The East";
	else
		eastDescription = "There Is No Room To The East";

	if (room->GetSouthRoom() != nullptr)
		southDescription = room->GetSouthRoom()->GetName() + " Is To The South";
	else
		southDescription = "There Is No Room To The South";

	if (room->GetWestRoom() != nullptr)
		westDescription = room->GetWestRoom()->GetName() + " Is To The West";
	else
		westDescription = "There Is No Room To The West";

	if (room->hasMonster())
		monsterDescription = "There Is A(n) " + room->GetMonster()->GetTypeName() + ", Named " + room->GetMonster()->GetName() + ", That Is " + room->GetMonster()->IsAliveString() + " In The Room";
	else
		monsterDescription = "There Is No Monster In The Room";

	//Actually Prints Out Discriptions
	cout << endl << "Description Of Room Called: " << room->GetName() << endl;

	cout << northDescription << endl;
	cout << eastDescription << endl;
	cout << southDescription << endl;
	cout << westDescription << endl;
	cout << monsterDescription << endl;
}

bool CheckVictory(Character* winner, Character* loser)
{
	if (loser->GetHitPoints() == 0)
	{
		cout << winner->GetName() << " has defeated " << loser->GetName() << endl;
		loser->Dead();
		return true;
	}
	else
		return false;
}
void AttackLoop(Character* attacker, Character* defender)
{
	Random rnd;

	while (!CheckVictory(attacker, defender) && !CheckVictory(defender, attacker))
	{
		if (!CheckVictory(defender, attacker))
			attacker->DoAttack(attacker, defender, rnd);

		if (!CheckVictory(attacker, defender))
			defender->DoAttack(defender, attacker, rnd);
	}
}

Room* exitRoom = new Room("The Exit");

bool EscapeCheck(Room* Exit)
{
	if (Exit == exitRoom)
	{
		cout << "CONGRATULATIONS!!!" << endl << "You've Escaped the Dungeon!!!" << endl;
		return true;
	}

	return false;
}

bool playing = true;
bool restart = true;

void GameplayAction(Player* player)
{
	string input;
	bool validInput = false;

	while (!validInput)
	{
		cout << "Select An Action: " << endl;

		cout << "1. Move North" << endl;
		cout << "2. Move East" << endl;
		cout << "3. Move South" << endl;
		cout << "4. Move West" << endl;
		cout << "5. Fight Monster" << endl;
		cout << "Q. Quit" << endl;

		getline(cin, input);
		if (input.length() >= 0)
		{
			if (input.length() < 2)
			{
				switch (input[0])
				{
				case '1':
				{
					Monster* monster = player->GetCurrentRoom()->GetMonster();
					if (player->GetCurrentRoom()->GetNorthRoom() != nullptr)
					{
						if (monster != nullptr && monster->IsAlive())
						{
							if (monster->GetSpeed() >= player->GetSpeed())
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " But You've Been Caught" << endl;
								AttackLoop(monster, player);
							}
							else
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " And Succeed, Slipping Out Of Its Grasp" << endl;
							}
						}

						player->SetCurrentRoom(player->GetCurrentRoom()->GetNorthRoom());
						cout << endl << "You Make Your Way North Into " << player->GetCurrentRoom()->GetName() << endl;

						validInput = true;
						break;
					}
					else
					{
						cout << endl << "There Is No Room To The North" << endl;

						validInput = false;
						break;
					}
				}
				case '2':
				{
					Monster* monster = player->GetCurrentRoom()->GetMonster();
					if (player->GetCurrentRoom()->GetEastRoom() != nullptr)
					{
						if (monster != nullptr && monster->IsAlive())
						{
							if (monster->GetSpeed() >= player->GetSpeed())
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " But You've Been Caught" << endl;
								AttackLoop(monster, player);
							}
							else
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " And Succeed, Slipping Out Of Its Grasp" << endl;
							}
						}

						player->SetCurrentRoom(player->GetCurrentRoom()->GetEastRoom());
						cout << endl << "You Make Your Way East Into " << player->GetCurrentRoom()->GetName() << endl;

						validInput = true;
						break;
					}
					else
					{
						cout << endl << "There Is No Room To The East" << endl;

						validInput = false;
						break;
					}
				}
				case '3':
				{
					Monster* monster = player->GetCurrentRoom()->GetMonster();
					if (player->GetCurrentRoom()->GetSouthRoom() != nullptr)
					{
						if (monster != nullptr && monster->IsAlive())
						{
							if (monster->GetSpeed() >= player->GetSpeed())
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " But You've Been Caught" << endl;
								AttackLoop(monster, player);
							}
							else
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " And Succeed, Slipping Out Of Its Grasp" << endl;
							}
						}
						player->SetCurrentRoom(player->GetCurrentRoom()->GetSouthRoom());
						cout << endl << "You Make Your Way South Into " << player->GetCurrentRoom()->GetName() << endl;

						validInput = true;
						break;
					}
					else
					{
						cout << endl << "There Is No Room To The South" << endl;

						validInput = false;
						break;
					}
				}
				case '4':
				{
					Monster* monster = player->GetCurrentRoom()->GetMonster();
					if (player->GetCurrentRoom()->GetWestRoom() != nullptr)
					{
						if (monster != nullptr && monster->IsAlive())
						{
							if (monster->GetSpeed() >= player->GetSpeed())
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " But You've Been Caught" << endl;
								AttackLoop(monster, player);
							}
							else
							{
								cout << endl << "You Try To Get Past " << monster->GetName() << " And Succeed, Slipping Out Of Its Grasp" << endl;
							}
						}

						player->SetCurrentRoom(player->GetCurrentRoom()->GetWestRoom());
						cout << endl << "You Make Your Way West Into " << player->GetCurrentRoom()->GetName() << endl;

						validInput = true;
						break;
					}
					else
					{
						cout << endl << "There Is No Room To The West" << endl;

						validInput = false;
						break;
					}
				}
				case '5':
				{
					Monster* monster = player->GetCurrentRoom()->GetMonster();
					if (monster != nullptr && monster->IsAlive())
					{
						AttackLoop(player, monster);

						validInput = true;
						break;
					}
					else
					{
						cout << endl << "There Is No Living Monster In The Room To Fight" << endl;

						validInput = false;
						break;
					}
				}
				case 'Q':
					playing = false;
					restart = false;
					validInput = true;
					break;
				case 'q':
					playing = false;
					restart = false;
					validInput = true;
					break;
				default:
					cout << "Invalid Option, Please Type The Corresponding Number To The Desired Action From The List." << endl << endl;
					validInput = false;
					break;
				}
			}
			else
			{
				cout << "Invalid Option, Please Type The Corresponding Number To The Desired Action From The List." << endl << endl;
				validInput = false;
			}
		}
	}

}
void GameOverOptions()
{
	string input;
	bool validInput = false;

	while (!validInput)
	{
		cout << "Select An Action: " << endl;

		cout << "1. Restart" << endl;
		cout << "Q. Quit" << endl << endl;

		getline(cin, input);
		if (input.length() >= 0)
		{
			if (input.length() < 2)
			{
				switch (input[0])
				{
				case '1':
					playing = true;
					restart = false;
					validInput = true;
					break;
				case 'Q':
					playing = false;
					restart = false;
					validInput = true;
					break;
				case 'q':
					playing = false;
					restart = false;
					validInput = true;
					break;
				default:
					cout << "Invalid Option, Please Type The Corresponding Number To The Desired Option From The List." << endl << endl;
					validInput = false;
					break;
				}
			}
		}
	}
}

static void ChooseAction(Player* player)
{

	if(player->IsAlive())
	{
		if (!EscapeCheck(player->GetCurrentRoom()))
			GameplayAction(player);
		else
			GameOverOptions();
	}
	else
	{
		player->PlayerDeath();
		GameOverOptions();
	}
}

int main(int argc, char* argv[])
{
	//GENERATING DUNGEON
	//-------------------------------------------------------------------------------------
	//Creating Rooms
	Room* startingRoom = new Room("The Starting Room");
	Room* pit = new Room("The Pit");
	Room* sunkenCity = new Room("The Sunken City");
	Room* hallway = new Room("A Hallway");
	Room* deepDark = new Room("The Deep Dark");
	Room* elevator = new Room("An Elevator");
	Room* ancestralMound = new Room("The Ancestral Mound");
	Room* soulSanctum = new Room("The Soul Sanctum");
	Room* restingGrounds = new Room("The Resting Grounds");
	Room* defiledDepths = new Room("The Defiled Depths");
	Room* abyss = new Room("The Abyss");

	//Establishing Map Layout
	startingRoom->SetSouthRoom(pit);
	startingRoom->SetEastRoom(ancestralMound);

	ancestralMound->SetWestRoom(startingRoom);
	ancestralMound->SetSouthRoom(soulSanctum);

	pit->SetNorthRoom(startingRoom);
	pit->SetSouthRoom(sunkenCity);
	pit->SetEastRoom(soulSanctum);

	soulSanctum->SetNorthRoom(ancestralMound);
	soulSanctum->SetWestRoom(pit);
	soulSanctum->SetEastRoom(restingGrounds);

	abyss->SetEastRoom(sunkenCity);

	sunkenCity->SetNorthRoom(pit);
	sunkenCity->SetWestRoom(abyss);
	sunkenCity->SetEastRoom(hallway);

	restingGrounds->SetWestRoom(soulSanctum);
	restingGrounds->SetSouthRoom(defiledDepths);

	hallway->SetWestRoom(sunkenCity);
	hallway->SetSouthRoom(deepDark);
	hallway->SetEastRoom(defiledDepths);

	defiledDepths->SetNorthRoom(restingGrounds);
	defiledDepths->SetWestRoom(hallway);

	deepDark->SetNorthRoom(hallway);
	deepDark->SetEastRoom(elevator);

	elevator->SetWestRoom(deepDark);
	elevator->SetNorthRoom(exitRoom);

	Room* rooms[] = {startingRoom, pit, sunkenCity, hallway, deepDark, elevator, exitRoom, ancestralMound, soulSanctum, restingGrounds, defiledDepths, abyss};
	
	//-------------------------------------------------------------------------------------


	

	while (playing)
	{
		//Variables For Replay Loop
		restart = true;
		bool validInput = false;
		//--------------------------

		cout << "Welcome To Dungeon Crawler!!" << endl;
		cout << "Goal: Find The Exit Room Alive" << endl;
		cout << "Press Enter To Begin...";
		cin.get();
		cout << endl;

		//GENERATING PLAYER
		//-------------------------------------------------------------------------------------
		//Establishing Player Attributes
		//Player Name
		string heroName;
		cout << "Please Enter Your Hero's Name: ";
		getline(cin, heroName);
		
		//Player Character Type
		string heroType;
		CharacterType type;
		bool validType = false;
		while (!validType)
		{
			cout << "1. Human" << endl;
			cout << "2. Ogre" << endl;
			cout << "3. Vampire" << endl;
			cout << "4. Minotaur" << endl;

			cout << "Please Choose Your Hero's Race: ";

			getline(cin, heroType);
			if (heroType.length() >= 0)
			{
				if (heroType.length() < 2)
				{
					switch (heroType[0])
					{
					case '1':
						type = CharacterType::Human;
						validType = true;
						break;
					case '2':
						type = CharacterType::Ogre;
						validType = true;
						break;
					case '3':
						type = CharacterType::Vampire;
						validType = true;
						break;
					case '4':
						type = CharacterType::Minotaur;
						validType = true;
						break;
					default:
						cout << "Invalid Option, Please Type The Corresponding Number To The Desired Race From The List." << endl << endl;
						validType = false;
						break;
					}
				}
				else
				{
					cout << "Invalid Option, Please Type The Corresponding Number To The Desired Race From The List." << endl << endl;
					validType = false;
				}
			}
		}


		Player* player = new Player(heroName, type);
		player->SetCurrentRoom(startingRoom);

		cout << endl << player->GetName() << endl;
		cout << player->GetTypeName() << endl;
		player->SetAlive(true);

		//-------------------------------------------------------------------------------------


		//FILLING DUNGEON
		//-------------------------------------------------------------------------------------
		//Generating Enemies
		CharacterType monsterTypes[] = { CharacterType::Human, CharacterType::Ogre, CharacterType::Vampire, CharacterType::Minotaur, CharacterType::Werewolf, CharacterType::Cerberus, CharacterType::Gravelord };
		Random rnd;
		for (int i = 0; i < 12; i++)
		{
			int monsterNumber = rnd.Integer(0, 6);
			int monsterChance = rnd.Integer(0, 100);
			if (monsterChance > 10 * (1 + (20 / 10)))
				rooms[i]->SetMonster(new Monster(monsterTypes[monsterNumber]));
		}
		//-------------------------------------------------------------------------------------

		//STARTING GAMEPLAY
		while (restart)
		{
			DescribeRoom(player->GetCurrentRoom());
			cout << endl << endl;
			ChooseAction(player);
		}
		delete player;
	}

	for (int i = 0; i < 12; i++)
	{
		delete rooms[i];
	}
}



