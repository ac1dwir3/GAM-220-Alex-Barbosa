#pragma once
#include "Character.h"
#include "Room.h"

class Player : public Character
{
public:
	Player()
		: Character("Placeholer Player Name", CharacterType::Human)
	{}
	Player(std::string newName)
		: Character(newName, CharacterType::Human)
	{}
	Player(std::string newName, CharacterType newType)
		: Character(newName, newType)
	{}

	Room* GetCurrentRoom()
	{
		return currentRoom;
	}
	void SetCurrentRoom(Room* newCurrentRoom)
	{
		currentRoom = newCurrentRoom;
	}

	void PlayerDeath()
	{
		if (!IsAlive())
			std::cout << std::endl << GetName() << " Has Died In The Dungeon..." << std::endl;
	}


private:
	Room* currentRoom = nullptr;
};