#include <iostream>
#include <string>
using namespace std;

string original = "'Twas the Night before Christmas and all through the land, Not a Saxon was stirring, nor Danish war band. A fire was burning in every Thane's hall, And a jolly Christmas Kingdom was hoped for by all.";
string stored = original;
bool quit = false;

void restoreOriginal()
{
	stored = original;
	cout << "String Restored" << endl << endl;
}
int getIndex()
{
	string indexInput;
	int index;
	getline(cin, indexInput);
	index = stoi(indexInput);

	return index;
}
void swapCharacters(int index1, int index2)
{
	string swapped = stored;
	index1--; index2--;
	if (index1 >= 0 && (unsigned)index1 < stored.length() && index2 >= 0 && (unsigned)index2 < stored.length())
	{
		char tempChar = swapped[index1];
		swapped[index1] = swapped[index2];
		swapped[index2] = tempChar;
		stored = swapped;
		cout << "Characters Swapped" << endl << endl;
	}
	else
	{
		cout << "Invalid Index Detected. Please Select New Indices. " << endl << "Please Enter Index Of First Character To Swap> ";
		index1 = getIndex();
		cout << "Please Enter Index Of Second Character To Swap> ";
		index2 = getIndex();
		cout << endl;
		swapCharacters(index1, index2);
	}
	
}
void toggleCap(int index)
{
	index--;
	string toggled = stored;
	if (index >= 0 && (unsigned)index < stored.length())
	{
		if (toggled[index] <= 122 && toggled[index] >= 97)
		{
			toggled[index] -= 32;
			stored = toggled;
			cout << "Character Capitalized" << endl << endl;
		}
		else if (toggled[index] <= 90 && toggled[index] >= 65)
		{
			toggled[index] += 32;
			stored = toggled;
			cout << "Character Lowercased" << endl << endl;
		}
	}
	else
	{
		cout << "Invalid Index. Please Choose A New Index> ";
		index = getIndex();
		cout << endl;
		toggleCap(index);
	}
}
void capFirst()
{
	string cappedF = stored;
	if (cappedF[0] <= 122 && cappedF[0] >= 97)
	{
		cappedF[0] -= 32;
	}
	for (unsigned int i = 0; i <= cappedF.length(); i++)
	{
		if (cappedF[i] == ' ')
		{
			if (cappedF[i+1] <= 122 && cappedF[i+1] >= 97)
			{
				cappedF[i+1] -= 32;
			}
		}
	}
	stored = cappedF;
	cout << "String Capitalized" << endl << endl;
}
void lowerAll()
{
	string lower = stored;
	for (unsigned int i = 0; i <= lower.length(); i++)
	{
		if (lower[i] <= 90 && lower[i] >= 65)
		{
			lower[i] += 32;
		}
	}
	stored = lower;
	cout << "String Lowercased" << endl << endl;
}
void capAll()
{
	string capped = stored;
	for (unsigned int i = 0; i <= capped.length(); i++)
	{
		if (capped[i] <= 122 && capped[i] >= 97)
		{
			capped[i] -= 32;
		}
	}
	stored = capped;
	cout << "String Capitalized" << endl << endl;
}
void reverseString()
{
	string reversed = stored;
	reverse(begin(reversed), end(reversed));
	stored = reversed;
	cout << "String Reversed" << endl << endl;
}
void appendString(string newString)
{
	stored = stored + " " + newString;
	cout << "New String Added" << endl << endl;
}
void setString(string newString)
{
	stored = newString;
	cout << "New String Set" << endl << endl;
}
 void displayString()
{
	cout << endl << "Current String: " << stored << endl << endl;
}

 void askUser()
{
	string input;

	//Used In Options b And c
	string newString;

	//Used In Option i
	int index1, index2;

	getline(cin, input);
	if (input.length() > 0)
	{
		switch (input[0])
		{

		case 'a':
			displayString();
			break;
		case 'A':
			displayString();
			break;
		case 'b':
			cout << "Please Enter New String> ";
			getline(cin, newString);
			setString(newString);
			break;
		case 'B':
			cout << "Please Enter New String> ";
			getline(cin, newString);
			setString(newString);
			break;
		case 'c':
			cout << "Please Enter String To Add> ";
			getline(cin, newString);
			appendString(newString);
			break;
		case 'C':
			cout << "Please Enter String To Add> ";
			getline(cin, newString);
			appendString(newString);
			break;
		case 'd':
			reverseString();
			break;
		case 'D':
			reverseString();
			break;
		case 'e':
			capAll();
			break;
		case 'E':
			capAll();
			break;
		case 'f':
			lowerAll();
			break;
		case 'F':
			lowerAll();
			break;
		case 'g':
			capFirst();
			break;
		case 'G':
			capFirst();
			break;
		case 'h':
			cout << "Please Enter Index Of Character To Toggle Capitalization> ";
			toggleCap(getIndex());
			cout << endl;
			break;
		case 'H':
			cout << "Please Enter Index Of Character To Toggle Capitalization> ";
			toggleCap(getIndex());
			cout << endl;
			break;
		case 'i':
			cout << "Please Enter Index Of First Character To Swap> ";
			index1 = getIndex();
			cout << "Please Enter Index Of Second Character To Swap> ";
			index2 = getIndex();
			swapCharacters(index1, index2);
			cout << endl;
			break;
		case 'I':
			cout << "Please Enter Index Of First Character To Swap> ";
			index1 = getIndex();
			cout << "Please Enter Index Of Second Character To Swap> ";
			index2 = getIndex();
			swapCharacters(index1, index2);
			cout << endl;
			break;
		case 'j':
			restoreOriginal();
			break;
		case 'J':
			restoreOriginal();
			break;
		case 'k':
			quit = true;
			break;
		case 'K':
			quit = true;
			break;
		}
	}
}

 void displayMenu()
{
	cout << "Please Choose An Option:" << endl
		 << "a) Display String" << endl
		 << "b) Set String" << endl
		 << "c) Append String" << endl
		 << "d) Reverse String" << endl
		 << "e) Capitalize All Characters" << endl
		 << "f) Lowercase All Characters" << endl
		 << "g) Capitalize Just The First Letter Of Each Word" << endl
		 << "h) Toggle Capitalization Of Character At Index" << endl
		 << "i) Swap Characters At Given Indices" << endl
		 << "j) Restore Original String" << endl
		 << "k) Quit" << endl;

	askUser();
}

int main(int argc, char* argv[])
{
	while (!quit)
	{
		displayMenu();
	}

	return 0;
}
