#include <iostream>;
using namespace std;

int main(int argc, char* argv[])
{
	int* intArray = new int[10];
	for (int i = 0; i < 10; i++)
	{
		intArray[i] = i+1;
	}

	int* movingPointer = intArray;

	cout << "++ operation increments a given value by 1" << endl;
	cout << "Memory Address Before Operation: " << movingPointer << endl;
	cout << "Size Of Integer: " << sizeof(*movingPointer) << " B" << endl;
	cout << "Value Before Operation: " << *movingPointer << endl;
	int memAddressBefore = (int)movingPointer;
	movingPointer++;
	int memAddressAfter = (int)movingPointer;
	cout << "Value After Operation: " << *movingPointer << endl;
	cout << "Memory Address After Operation: " << movingPointer << endl;
	int difference = memAddressAfter - memAddressBefore;
	cout << "Difference In Memory Address In Bytes: " << difference << " B" << endl;

	cout << endl;

	cout << "-- operation decrements a given value by 1" << endl;
	cout << "Memory Address Before Operation: " << movingPointer << endl;
	cout << "Size Of Integer: " << sizeof(*movingPointer) << " B" << endl;
	cout << "Value Before Operation: " << *movingPointer << endl;
	memAddressBefore = (int)movingPointer;
	movingPointer--;
	memAddressAfter = (int)movingPointer;
	cout << "Value After Operation: " << *movingPointer << endl;
	cout << "Memory Address After Operation: " << movingPointer << endl;
	difference = memAddressAfter - memAddressBefore;
	cout << "Difference In Memory Address In Bytes: " << difference << " B" << endl;

	cout << endl;

	cout << "+= operation adds a given value with another value. useful for incrementing by numbers larger than 1. example: num += 5 would result in num = num+5; " << endl;
	cout << "Memory Address Before Operation: " << movingPointer << endl;
	cout << "Size Of Integer: " << sizeof(*movingPointer) << " B" << endl;
	cout << "Value Before Operation: " << *movingPointer << endl;
	memAddressBefore = (int)movingPointer;
	movingPointer += 5;
	memAddressAfter = (int)movingPointer;
	cout << "Value After Operation: " << *movingPointer << endl;
	cout << "Memory Address After Operation: " << movingPointer << endl;
	difference = memAddressAfter - memAddressBefore;
	cout << "Difference In Memory Address In Bytes: " << difference << " B" << endl;

	delete[] intArray;
	return 0;
}