#pragma once
#include <string>

class Weapon;

class Hero
{
public:
	Hero()
	{
		name = "Hero";
		hitpoints = 100;
		weapon = nullptr;
	}
	Hero(std::string newName, int newHitpoints, Weapon* newWeapon)
		: name(newName), hitpoints(newHitpoints), weapon(newWeapon)
	{}

	void Hurt(int damage)
	{
		if (damage < 0)
			damage = 0;

		hitpoints -= damage;
		if (hitpoints < 0)
			hitpoints = 0;
	}

	std::string GetName() const
	{
		return name;
	}

	void SetHitPoints(int newHitPoints)
	{
		hitpoints = newHitPoints;
	}
	int GetHitpoints() const
	{
		return hitpoints;
	}

	void SetWeapon(Weapon* newWeapon)
	{
		weapon = newWeapon;
	}
	Weapon* GetWeapon() const
	{
		return weapon;
	}


private:
	std::string name;
	int hitpoints;
	Weapon* weapon;
};