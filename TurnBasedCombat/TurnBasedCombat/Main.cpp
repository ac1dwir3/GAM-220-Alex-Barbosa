#include <iostream>
#include <string>
#include "Hero.h"
#include "Scimitar.h"
#include "MorningStar.h"
#include "FistRope.h"
using namespace std;

bool CheckVictory(Hero& winner, Hero& loser)
{
	if (loser.GetHitpoints() == 0)
	{
		cout << winner.GetName() << " has defeated " << loser.GetName() << endl;
		return true;
	}
	else
		return false;
}

int main(int argc, char* argv[])
{
	bool playing = true;

	while (playing)
	{
		//Variables For Replay Loop
		bool rematch = true;
		bool validInput = false;
		//--------------------------

		string hero1Name, hero2Name;
		int hero1WeaponChoice, hero2WeaponChoice = 0;
		string input;

		Scimitar* scimitar = new Scimitar(20, 20);
		MorningStar* morningstar = new MorningStar(35, 5);
		FistRope* fistrope = new FistRope(30, 10);

		Weapon* weapons[3] = { scimitar, morningstar, fistrope };

		cout << "Hero 1, Type In Your Name: ";
		getline(cin, hero1Name);
		cout << endl;

		cout << "Hero 2, Type In Your Name: ";
		getline(cin, hero2Name);
		cout << endl;

		Hero hero1(hero1Name, 100, nullptr);
		Hero hero2(hero2Name, 100, nullptr);

		cout << "Weapons: " << endl;
		for (int i = 0; i < 3; i++)
			cout << i + 1 << ". " << weapons[i]->GetName() << endl;
		cout << endl;

		while (hero1.GetWeapon() == nullptr)
		{
			cout << hero1Name << " Choose Your Weapon: ";
			getline(cin, input);
			if (input.length() >= 0)
			{
				switch (input[0])
				{

				case '1':
					hero1WeaponChoice = stoi(input) - 1;
					hero1.SetWeapon(weapons[hero1WeaponChoice]);
					break;
				case '2':
					hero1WeaponChoice = stoi(input) - 1;
					hero1.SetWeapon(weapons[hero1WeaponChoice]);
					break;
				case '3':
					hero1WeaponChoice = stoi(input) - 1;
					hero1.SetWeapon(weapons[hero1WeaponChoice]);
					break;
				default:
					cout << "Invalid Weapon, Please Type The Corresponding Number To The Desired Weapon From The List." << endl << endl;
					break;
				}
			}
		}

		cout << endl;

		while (hero2.GetWeapon() == nullptr)
		{
			cout << hero2Name << " Choose Your Weapon: ";
			getline(cin, input);
			if (input.length() >= 0)
			{
				switch (input[0])
				{

				case '1':
					hero2WeaponChoice = stoi(input) - 1;
					hero2.SetWeapon(weapons[hero2WeaponChoice]);
					break;
				case '2':
					hero2WeaponChoice = stoi(input) - 1;
					hero2.SetWeapon(weapons[hero2WeaponChoice]);
					break;
				case '3':
					hero2WeaponChoice = stoi(input) - 1;
					hero2.SetWeapon(weapons[hero2WeaponChoice]);
					break;
				default:
					cout << "Invalid Weapon, Please Type The Corresponding Number To The Desired Weapon From The List." << endl;
					break;
				}
			}
		}

		cout << endl;

		while (rematch)
		{
			Random rnd;
			int round = 1;

			hero1.SetHitPoints(100);
			hero2.SetHitPoints(100);

			while (!CheckVictory(hero1, hero2) && !CheckVictory(hero2, hero1))
			{
				cout << "Turn " << round << endl;
				cout << "_________________________________" << endl << endl;
				if (hero1.GetWeapon()->GetSpeed() >= hero2.GetWeapon()->GetSpeed())
				{
					if (!CheckVictory(hero2, hero1))
						hero1.GetWeapon()->DoAttack(hero1, hero2, rnd);

					if (!CheckVictory(hero1, hero2))
						hero2.GetWeapon()->DoAttack(hero2, hero1, rnd);
					round++;
				}
				else
				{
					if (!CheckVictory(hero1, hero2))
						hero2.GetWeapon()->DoAttack(hero2, hero1, rnd);

					if (!CheckVictory(hero2, hero1))
						hero1.GetWeapon()->DoAttack(hero1, hero2, rnd);
					round++;
				}
				cout << "---------------------------------" << endl << endl;
			}


			cout << endl;

			while (!validInput)
			{
				cout << "1. Restart" << endl;
				cout << "2. Rematch" << endl;
				cout << "Q. Quit" << endl << endl;

				getline(cin, input);
				if (input.length() >= 0)
				{
					if (input.length() < 2)
					{
						switch (input[0])
						{
						case '1':
							playing = true;
							rematch = false;
							validInput = true;
							break;
						case '2':
							playing = true;
							rematch = true;
							validInput = true;
							break;
						case 'Q':
							playing = false;
							rematch = false;
							validInput = true;
							break;
						case 'q':
							playing = false;
							rematch = false;
							validInput = true;
							break;
						default:
							cout << "Invalid Option, Please Type The Corresponding Number To The Desired Option From The List." << endl << endl;
							validInput = false;
							break;
						}
					}
				}
				else
				{
					cout << "Invalid Option, Please Type The Corresponding Number To The Desired Option From The List." << endl << endl;
					validInput = false;
				}
			}
			validInput = false;
		}
	}

	return 0;
}