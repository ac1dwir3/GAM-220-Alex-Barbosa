#pragma once
#include "Weapon.h"
#include <iostream>
#include "Random.h"

class MorningStar : public Weapon
{
public:
	MorningStar(int newPower, int newSpeed)
		: Weapon(newPower, newSpeed, WeaponType::Bludgeoning)
	{}

	virtual std::string GetName() const override
	{
		return "Morningstar";
	}

	virtual void DoAttack(Hero& attacker, Hero& defender, Random& rnd) override
	{
		int randomNumber = rnd.Integer(0, 100);
		switch (defender.GetWeapon()->GetType())
		{
			case WeaponType::Slashing:
				if (randomNumber < 10 * (1 + (defender.GetWeapon()->GetSpeed() / 10)))
				{
					std::cout << attacker.GetName() << " swings the " << GetName() << ", but it is blocked!" << std::endl;
					return;
				}
				break;
			case WeaponType::Bludgeoning:
				if (randomNumber < 20 * (1 + (defender.GetWeapon()->GetSpeed() / 10)))
				{
					std::cout << attacker.GetName() << " swings the " << GetName() << ", but it is blocked!" << std::endl;
					return;
				}
				break;
			case WeaponType::Piercing:
				if (randomNumber < 5 * (1 + (defender.GetWeapon()->GetSpeed() / 10)))
				{
					std::cout << attacker.GetName() << " swings the " << GetName() << ", but it is blocked!" << std::endl;
					return;
				}
				break;
		}

		// Gets Damage
		Weapon* w = attacker.GetWeapon();
		int damage = w->GetPower();

		// Announce the attack
		std::cout << attacker.GetName() << " slams the " << GetName() << " into ";
		std::cout << defender.GetName() << "'s skull, striking for " << damage << " damage!  ";

		// Make it hurt
		defender.Hurt(damage);

		// Say what the effect was
		std::cout << defender.GetName() << " has " << defender.GetHitpoints() << " health left." << std::endl;
	}
};