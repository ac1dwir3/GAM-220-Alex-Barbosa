#pragma once
#include "Hero.h"
#include "Random.h"

enum class WeaponType
{
	Slashing,
	Piercing,
	Bludgeoning
};

class Weapon
{
public:
	Weapon(int newPower, int newSpeed, WeaponType newType)
	{
		power = newPower;
		speed = newSpeed;
		type = newType;
	}

	int GetPower() const
	{
		return power;
	}
	int GetSpeed() const
	{
		return speed;
	}
	WeaponType GetType() const
	{
		return type;
	}

	virtual std::string GetName() const abstract;

	virtual void DoAttack(Hero& attacker, Hero& defender, Random& rnd) abstract;

protected:
	int power;
	int speed;
	WeaponType type;
};
